import Vue from 'vue'
import './plugins/axios'


import App from './App.vue'
import router from './router'
import store from './store'
import Vant, {Col, Icon, Row, Search, Toast} from 'vant';
import 'vant/lib/index.css';
import http from "../http"
import 'video.js/dist/video-js.css'
import '@/assets/style.css'
import '@/assets/font_af0lx0ts88/iconfont.css'
import '@/assets/font/iconfont.css'
import axios from "axios";
import Mui from 'vue-awesome-mui';
import io from "./plugins/socket.io";


Vue.use(Mui);
Vue.use(Col);
Vue.use(Row);
Vue.use(Search);
Vue.use(Icon);
Vue.prototype.$msg = Toast
Vue.use(Toast);
Vue.prototype.$http = http
Vue.use(Vant);
Vue.config.productionTip = false
const http2 = axios.create({
        // baseURL: "http://112.74.99.5:3000/web/api"
        // baseURL: "http://47.94.138.254"
        baseURL: "/novel"
    }
)

String.prototype.format = function () {
    if (arguments.length == 0) return this;
    var param = arguments[0];
    var s = this;
    if (typeof (param) == 'object') {
        for (var key in param)
            s = s.replace(new RegExp("\\{" + key + "\\}", "g"), param[key]);
        return s;
    } else {
        for (var i = 0; i < arguments.length; i++)
            s = s.replace(new RegExp("\\{" + i + "\\}", "g"), arguments[i]);
        return s;
    }
}
Vue.prototype.$http2 = http2
const url = 'http://127.0.0.1:8081?token=' + localStorage.getItem('token');
const socket = io.connect(url)
socket.on("email", function (data) {
    store.commit('sentToEmails', data)
})
socket.on('follower', (data) => {
    console.log(data)
})
socket.on('like', (data) => {
    store.commit('sendToLikes', [data])
})
Vue.prototype.$socket = socket
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

