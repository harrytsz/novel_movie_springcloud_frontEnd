import Vue from 'vue'
import Vuex from 'vuex'
import http from "../../http";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        emails: [],
        totalCheck: 0,
        likeCheck: 0,
        likes: [],
        unCheckLikes: []
    },
    mutations: {
        async sentToEmails(state, emails) {
            console.log(emails)
            let sum = 0
            emails.forEach((x) => {
                sum += x.needCheck
            })
            console.log(sum)
            state.totalCheck = sum
            state.emails = emails
        },
        sendToLikes(state, likes) {
            let sum = 0
            likes.forEach((x) => {
                if (!x.checked) {
                    sum += !x.checked
                    state.unCheckLikes.push(x)
                }
                state.likes.push(x)
            })
            state.likeCheck = sum
        },
        checkLikes(state) {
            state.likeCheck = 0
        }
    },
    actions: {
        asyncCheckLikes(context) {
            context.state.unCheckLikes.forEach((x) => {
                x.checked = true
            })
            http.post("/collect/updateCollects", context.state.unCheckLikes).then((res) => {
                if (res.data.code === 0) {
                    context.commit('checkLikes')
                }
            })
        }
    },
    modules: {}
})
