// module.exports = {
//     devServer: {
//         port: 80,   // 端口号
//         disableHostCheck: true
//     },
//     publicPath:'./',
// };
const path = require('path');

const webpack = require('webpack')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const productionGzipExtensions = ['js', 'css']
const isProduction = process.env.NODE_ENV === 'production'
module.exports = {
    lintOnSave: false,
    runtimeCompiler: true,
    publicPath: './',
    devServer: {
        disableHostCheck: true,
        open: true,
        port: 82,
        proxy: {
            '/api': {
                // target: 'http://47.92.95.86:8080',
                target: 'http://localhost:81',
                changeOrigin: true,
                ws: true,
                secure: true,
                pathRewrite: {
                    '^/api': ''
                }
            },
            '/movie': {
                target: 'http://localhost:5001',
                changeOrigin: true,
                ws: true,
                secure: true,
                pathRewrite: {
                    '^/movie': ''
                }
            },
            '/novel': {
                target: 'http://localhost:5000',
                changeOrigin: true,
                ws: true,
                secure: true,
                pathRewrite: {
                    '^/novel': ''
                }
            },
            '/face': {
                target: 'http://localhost:5000',
                changeOrigin: true,
                ws: true,
                secure: true,
                pathRewrite: {
                    '^/face': ''
                }
            },
            '/resource': {
                target: "http://localhost:9090",
                changeOrigin: true,
                ws: true,
                secure: true,
                pathRewrite: {
                    '^/resource': ''
                }
            }
        }
    },
    configureWebpack: {
        resolve: {
            alias: {
                '@': path.resolve(__dirname, './src'),
                '@i': path.resolve(__dirname, './src/assets'),
            }
        },
        plugins: [
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

            // 下面是下载的插件的配置
            new CompressionWebpackPlugin({
                algorithm: 'gzip',
                test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
                threshold: 10240,
                minRatio: 0.8
            }),
            new webpack.optimize.LimitChunkCountPlugin({
                maxChunks: 5,
                minChunkSize: 100
            })
        ]
    }
};